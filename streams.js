var Streams = {
    data: [],
    sortBy: undefined,
    indexName: undefined,
    pageStart: 0,
    pageEnd: undefined,
    pages: 0,
    page: 0,
    length: 0
};

Streams.forEach = function(functionCallback) {

    Streams.length = Streams.data.length;
    Streams.pages = Math.ceil((Streams.length/(Streams.pageEnd-Streams.pageStart)));
    Streams.page = ((Streams.pageStart/(Streams.pageEnd-Streams.pageStart))+1);

    if(!Streams.pageEnd) { Streams.pageEnd = Streams.length; }

    Streams.data = Streams.data.slice(Streams.pageStart,Streams.pageEnd);           
        
    Streams.data.sort(Streams.sortByColumn);
    
    for(var key in Streams.data) {
        var data = Streams.data[key];
        if(Streams.indexName) { data[Streams.indexName] = parseInt(key); }
        functionCallback(data);
    }
    
}

Streams.sortWith = function(sortBy) {
    Streams.sortBy = sortBy;
    return Streams;
}

Streams.stream = function(data) {
    Streams.data = JSON.parse(data);
    Streams.sortBy = undefined;
    Streams.filterBy = undefined;
    Streams.indexName = undefined;
    return Streams;
}

Streams.filter = function(filterBy) {
            
    var newData = [];
    
    for(var key in Streams.data) {
        var data = Streams.data[key];
        if(filterBy(data)) {
            newData.push(data);
        }
    }
    
    Streams.data = newData;
    
    return Streams;
}

Streams.index = function(indexName) {
    Streams.indexName = indexName;
    return Streams;
}

Streams.paginate = function(pageStart,pageEnd) {
    Streams.pageStart = (pageStart-1);
    Streams.pageEnd = (pageEnd-1);
    return Streams;
}

Streams.sortByColumn = function(x,y) {
    return ((x[Streams.sortBy]  == y[Streams.sortBy]) ? 0 : ((x[Streams.sortBy]> y[Streams.sortBy]) ? 1 : -1 ));
}

